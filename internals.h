/*
 * This file contains internal commands of shell.
 * It contains function to initial configuration, to decide whether
 * there is a internal command of given name
 * and to execute the internal command.
 *
 * Created by Vít Kabele on 2018-12-13.
 */

#ifndef MYSH_INTERNALS_H
#define	MYSH_INTERNALS_H

#define	_XOPEN_SOURCE 600
#include <unistd.h>
#include <sys/queue.h>

struct pair_entry {
	char * name;
	int (*function)(int argc, char ** argv);
	SLIST_ENTRY(pair_entry) entries;
};

void
register_internals();

void
deregister_internals();

/*
 * Return 1 in case that the command is internal, 0 otherwise
 */
int
is_internal(const char * name);

/*
 * Run shell internal command
 *
 * Return result of the command (0-127).
 *
 * RETURN VALUES
 * 		0-127	The result of command.
 *
 */
int
run_internal(int argc, char ** argv);

#endif // MYSH_INTERNALS_H

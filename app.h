/*
 * This file represents the program entry point delegated from main function.
 * Contains definition of shell state struct and defines its global variable.
 * Also contains cleanup procedure.
 *
 * Created by Vít Kabele on 26/11/2018.
 */

#ifndef	MYSH_APP_H
#define	MYSH_APP_H

#define	_XOPEN_SOURCE 600
#include <unistd.h>

#include <stdio.h>

#include "common.h"

/*
 * The state of the shell.
 * PS1, PWD, OLDPWD etc.
 */
struct state {
	char * PS1;
	char * PWD;
	char * OLDPWD;
	int is_batch;
	int last_command_return_value;
};

/*
 * Global state
 */
struct state shell_state;

/*
 * Run the app
 * @return
 */
int
run(int argc, char ** argv);

int
cleanup_and_die();

#endif // MYSH_APP_H

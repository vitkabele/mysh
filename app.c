/*
 * This file represents the program entry point delegated from main function.
 * Contains definition of shell state struct and defines its global variable.
 * Also contains cleanup procedure.
 *
 * Created by Vít Kabele on 26/11/2018.
 */

#include <unistd.h>
#include <getopt.h>

#include <err.h>
#include <assert.h>

#include <stdio.h>
#include <readline/readline.h>
#include <readline/history.h>

#include <stdlib.h>

#include <signal.h>

#include "common.h"
#include "app.h"
#include "internals.h"
#include "ast.h"
#include "mysh.tab.h"
#include "lex.yy.h"

extern FILE * yyin;

extern int yyparse();

extern YY_BUFFER_STATE yy_scan_string(const char * f);
extern void yy_delete_buffer(YY_BUFFER_STATE b);

/*
 * Initial configuration
 */
static void
initial_config()
{
	shell_state.PS1 = "mysh";
	shell_state.last_command_return_value = 0;
	register_internals();
	shell_state.is_batch = 0;
}

/*
 * Run single line
 * @param line
 * @return
 */
static int
run_line(const char * line)
{
	char * buffer = malloc(strlen(line) + 2);
	assert(buffer != NULL);

	sprintf(buffer, "%s\n", line);

	YY_BUFFER_STATE  buffer_state = yy_scan_string(buffer);
	yyparse();
	yy_delete_buffer(buffer_state);

	free(buffer);

	return (shell_state.last_command_return_value);
}

/*
 * Handle signals while processing command
 */
static void
handle_signals(int signo)
{
	if (signo == SIGINT) {
#ifndef __APPLE__
		// Some of those functions are not available
		// in apple implementation of GNU readline.
		// To achieve valid behavior a platform specific
		// implementation will be required
		rl_on_new_line_with_prompt(); // Regenerate the prompt
		rl_replace_line("", 0); // Clear the previous text
		rl_redisplay();
#endif
	}
}

/*
 * Handle signals while in readline
 */
static void
handle_signals_rl(int signo)
{
	if (signo == SIGINT) {
#ifndef __APPLE__
		// Some of those functions are not available
		// in apple implementation of GNU readline.
		// To achieve valid behavior a platform specific
		// implementation will be required
		rl_crlf();
		rl_on_new_line(); // Regenerate the prompt on a newline
		rl_replace_line("", 0); // Clear the previous text
		rl_redisplay();
#endif
	}
}

/*
 * Run interactive shell
 */
static void
run_interactive()
{

	char * line;
	struct sigaction act = { {0} };
	struct sigaction rl = {{0}};
	size_t prompt_buffer_size = strlen(shell_state.PS1) + PATH_MAX + 32;
	char * prompt = malloc(prompt_buffer_size * sizeof (char));
	assert(prompt != NULL); // Fatal error -> die

	act.sa_handler = handle_signals;
	act.sa_flags = SA_RESTART;

	rl.sa_handler = handle_signals_rl;
	rl.sa_flags = SA_RESTART;

	int t = sigaction(SIGINT, &rl, NULL);
	if (t == -1) {
		free(prompt);
		err(EX_OSERR, NULL);
	}

	sprintf(prompt, PROMPT_FMT, shell_state.PS1, shell_state.PWD);
	while ((line = readline(prompt)) != NULL) {
		t = sigaction(SIGINT, &act, NULL);
		if (t == -1) {
			free(prompt);
			err(EX_OSERR, NULL);
		}

		(void) run_line(line);

		if (strcmp(line, "") != 0) {
			add_history(line);
		}

		free(line);
		sprintf(prompt, PROMPT_FMT, shell_state.PS1, shell_state.PWD);

		t = sigaction(SIGINT, &rl, NULL);
		if (t == -1) {
			free(prompt);
			err(EX_OSERR, NULL);
		}
	}

	free(prompt);
	cleanup_and_die();
}

/*
 * Run batch file
 * @param file
 * @return
 */
static void
run_batch(const char * file)
{

	shell_state.is_batch = 1;

	FILE * fp = fopen(file, "r");

	assert(fp != NULL);

	yyin = fp;

	yyparse();

	fclose(fp);

	cleanup_and_die();
}


/*
 * Free resources and exit the shell
 */
int
cleanup_and_die()
{
	deregister_internals();
	exit(shell_state.last_command_return_value);
}

/*
 * This is i.e. main function
 * @param argc
 * @param argv
 * @return
 */
int
run(int argc, char ** argv)
{

	initial_config();

	int ch;
	while ((ch = getopt(argc, argv, "c:")) != -1) {
		switch (ch) {
		case 'c':
			(void) run_line(optarg);
			cleanup_and_die();
		default:
			err(1, "Unknown option: %c", ch);
		}
	}

	if (argc == 2)
		run_batch(argv[1]);

	run_interactive();
	return (0);
}

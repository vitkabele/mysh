/*
 * This file contains macros etc. that are used in whole program.
 * As this header is included in every single file of project
 * can be used as simple parameter storage and thus to simple compile time
 * configuring of the project.
 */
#ifndef _COMMON_H
#define	_COMMON_H

#include <sys/types.h>
#include <string.h>
#include <limits.h>

#include <errno.h>
#include <sysexits.h>
#include <err.h>


// Create new reference to struct of type 'type', named 'name'
#define	CONSTRUCT(type, name) \
		struct type * name = malloc(sizeof (struct type)); \
		if (name == NULL) { err(EX_OSERR, NULL); }

#define	ERRNO_EXIT(condition, errnoval, retval) \
		if (condition) {\
			errno = errnoval; \
			return (retval); \
		}

#define	ERRNO_EXIT_CLEANUP(condition, errnoval, retval, cleanup_routine) \
		if (condition) {\
			cleanup_routine; \
			errno = errnoval; \
			return (retval); \
		}

// Base number to be added to signal number
// in terminated command return value
#define	SIGNAL_RETURN_BASE 128


#define	WARN_PREFIX "[mysh]:"

// Prompt format "PS1:PWD$"
#define	PROMPT_FMT "%s:%s$ "

#endif // _COMMON_H

/*
 * Execution engine of mysh.
 * Perform execution of abstract syntax tree representing command line.
 *
 * Created by Vít Kabele on 2018-12-13.
 */

#include <fcntl.h>
#include <assert.h>
#include <err.h>
#include <stdlib.h>
#include <memory.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <string.h>
#include <sys/stat.h>
#include <errno.h>
#include <sysexits.h>

#include "common.h"
#include "ee.h"
#include "internals.h"
#include "app.h"

/*
 * Create an array from command struct.
 *
 * RETURN VALUES
 * 		Returns the pointer to the array of size cmd->arg_count
 * 		or NULL on failure. In case of failure, errno is set to indicate
 * 		the error.
 *
 * ERRORS
 * 		[ENOMEM]		Not enough space to allocate the array.
 *
 * WARNING
 * 		Remember to free the return value
 */
static char **
args_to_array(const Command * cmd)
{

	char ** 	array;
	int 		i = 0;
	struct string_entry * arg;

	array = malloc((cmd->arg_count + 1) * sizeof (char *));
	ERRNO_EXIT(array == NULL, ENOMEM, NULL);


	STAILQ_FOREACH(arg, &cmd->args, entries) {
		array[i++] = arg->item;
	}
	array[i] = NULL;

	return (array);
}

/*
 * The procedure that will run in forked process before execvp.
 *
 * ERRORS
 * 		Error in this function will lead to leaving the child process
 * 		and the exit code will be treated as return value of command.
 *
 */
static void
child(const Command * cmd, int in, int out, char * const * argv)
{
	int of = out;
	int inf = in;

	// Input redirection
	if (cmd->srcname != NULL) {
				inf = open(
						cmd->srcname,
						O_RDONLY);

				if (inf == -1) {
					err(EX_OSERR, NULL);
				}

			}
	dup2(inf, 0);

	// Output redirection
	if (cmd->destname != NULL) {
				of = open(
					cmd->destname,
					O_CREAT | O_WRONLY |
					(cmd->append ? O_APPEND : 0),
					S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH);

				if (of == -1) {
					err(EX_OSERR, NULL);
				}

			}
	dup2(of, 1);

	execvp(argv[0], argv);
	err(127, "%s", argv[0]);
}

/*
 * Run the command. Determine whether it is internal one or not and handle it
 * accordingly. If the command failed to run, return value is set to -1 and
 * the global variable errno is set to indicate the error.
 *
 * RETURN VALUE
 * 		-1		On failure.
 * 		0		When internal command was executed.
 * 		Return value in second arg.
 * 		PID		PID of executed subprocess. Positive integer.
 *
 * ERRORS
 * 		[ENOMEM]	Not enough memory to handle the command.
 *
 */
static int
execute_command(Command * cmd, int * return_value, int in, int out)
{

	assert(cmd->arg_count > 0); // Fatal error. Probably in parser
	assert(return_value != NULL);	// Improper function call

	char ** argv = args_to_array(cmd);
	ERRNO_EXIT(argv == NULL, ENOMEM, -1);

	if (is_internal(argv[0])) {
		(*return_value) = run_internal(cmd->arg_count, argv);
		free(argv);
		return (0);
	} else {

		int chpid = fork();

		if (chpid == -1) {
			switch (errno) {
			case ENOMEM:
				fprintf(stderr, "Fork failed because of "
					"insufficient swap space\n");
				break;
			case EAGAIN:
				fprintf(stderr, "The system/user limit on the "
		    "total proccess number was exceeded.\n");
				break;
			default:
				fprintf(stderr, "Unknown error "
		    "during fork.\n");
				err(EX_OSERR, NULL);
			}
			return (-1);
		}

		// Child process
		if (chpid == 0) {

			child(cmd, in, out, argv);

		}

		free(argv);
		return (chpid);

	}
}

/*
 * Execute the pipeline.
 *
 * 1) Tie the commands together via pipes
 * 2) Perform the file redirection
 * run them.
 * 4) Wait for all of them
 * 5) Determine the exit code of last command
 */
static int
execute_pipeline(Pipeline * pipeline)
{

	struct command_entry * cmd;
	int 	fd[2] = { STDIN_FILENO, STDOUT_FILENO };
	int 	wstatus;
	int 	options = 0;
	int 	retval = 0;
	int 	pid = 0;

	STAILQ_FOREACH(cmd, &pipeline->commands, entries) {

		int indes = fd[0];
		int outdes;

		if (STAILQ_NEXT(cmd, entries) == NULL) {
			outdes = STDOUT_FILENO;
		} else {

			// Skip cd and exit command if they are not last
			if (is_internal(cmd->command->args.stqh_first->item)) {
				continue;
			}

			if (pipe(fd) == -1) {
				err(EX_OSERR, "Failed to create pipe.");
			}
			outdes = fd[1];
		}

		pid = execute_command(cmd->command, &retval, indes, outdes);

		if (indes != STDIN_FILENO) {
			close(indes);
		}

		if (outdes != STDOUT_FILENO) {
			close(outdes);
		}

	}

	// Wait for non-internal commands
	if (pid > 0) {
		waitpid(pid, &wstatus, options);

		if (WIFEXITED(wstatus)) {
			retval = WEXITSTATUS(wstatus);
		} else if (WIFSIGNALED(wstatus)) {
			fprintf(stderr, "Killed by signal %d\n",
				WTERMSIG(wstatus));
			retval = SIGNAL_RETURN_BASE + WTERMSIG(wstatus);
		} else if (WIFSTOPPED(wstatus)) {
			fprintf(stderr, "Stopped by signal %d\n",
				WSTOPSIG(wstatus));
			retval = SIGNAL_RETURN_BASE + WSTOPSIG(wstatus);
		} else {
			fprintf(stderr, "Unkonwn exit status");
			retval = 126;
		}
	}

	while(wait(NULL) != -1);

	shell_state.last_command_return_value = retval;
	return (retval);
}

void
execute_sequence(Sequence * sequence)
{

	struct pipeline_entry * pipeline;
	STAILQ_FOREACH(pipeline, &sequence->pipelines, entries) {
		execute_pipeline(pipeline->pipeline);
	}

}

%option noyywrap nodefault yylineno nounput noinput

%{
#include "ast.h"
#include "mysh.tab.h"
#include "app.h"
%}

%%
[-$)(/\_.a-zA-Z0-9]+        { yylval.str = yytext ; return TOKEN; }
;                           { return SEMICOLON; }
\|                          { return PIPE; }
>>                          { return APPEND; }
>                           { return REDIR_OUT; }
\<                          { return REDIR_IN; }
\n                          { return EOL; }
#[^\n]*                     { }
.                           { }
%%

int
main(int argc, char ** argv)
{
    return run(argc, argv);
}

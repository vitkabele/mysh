/*
 * This file contains internal commands of shell.
 * It contains function to initial configuration, to decide whether
 * there is a internal command of given name
 * and to execute the internal command.
 *
 * Created by Vít Kabele on 2018-12-13.
 */

#include <stdlib.h>
#include <string.h>
#include <err.h>
#include <sysexits.h>
#include <unistd.h>
#include <limits.h>

#include "common.h"
#include "ast.h"
#include "app.h"
#include "internals.h"

SLIST_HEAD(slist_head, pair_entry) internals_head =
		SLIST_HEAD_INITIALIZER(internals_head);

int
internal_exit(int argc, char ** argv)
{
	return (cleanup_and_die());
}

// _GNU_SOURCE is guard macro for function with the same name.
#ifndef _GNU_SOURCE
char *
get_current_dir_name(void)
{

	char * buffer = malloc(PATH_MAX * sizeof (char));

	if (getcwd(buffer, PATH_MAX) == NULL) {
		err(EX_SOFTWARE, "Current working dir path "
			"exceeds the PATH_MAX size!");
	}

	return (buffer);

}
#endif

void
init_cd()
{
	shell_state.PWD = get_current_dir_name();
	shell_state.OLDPWD = get_current_dir_name();
}

int
internal_cd(int argc, char ** argv)
{

	char * DEST = NULL;
	// cd: go home
	if (argc == 1) {
		DEST = getenv("HOME");
	} else if (argc == 2 && (strcmp(argv[1], "-") == 0)) {
		DEST = shell_state.OLDPWD;
	} else if (argc == 2) {
		DEST = argv[1];
	} else {
		printf("Invalid usage of cd command\n");
		return (1);
	}

	switch (chdir(DEST)) {
		case 0:
			free(shell_state.OLDPWD);
			shell_state.OLDPWD = shell_state.PWD;
			shell_state.PWD = get_current_dir_name();
			return (0);
		default:
			fprintf(stderr, "Cannot switch to %s\n", DEST);
			return (1);
	}

}


void
register_internals()
{

	CONSTRUCT(pair_entry, exit);
	exit->name = "exit";
	exit->function = internal_exit;
	SLIST_INSERT_HEAD(&internals_head, exit, entries);

	CONSTRUCT(pair_entry, cd);
	cd->name = "cd";
	cd->function = internal_cd;
	init_cd();
	SLIST_INSERT_HEAD(&internals_head, cd, entries);


}

void
deregister_internals()
{

	struct pair_entry * entry;

	while (!SLIST_EMPTY(&internals_head)) {
		entry = SLIST_FIRST(&internals_head);
		SLIST_REMOVE_HEAD(&internals_head, entries);
		free(entry);
	}

	free(shell_state.OLDPWD);
	free(shell_state.PWD);

}

int
is_internal(const char * name)
{

	struct pair_entry * entry;
	SLIST_FOREACH(entry, &internals_head, entries) {
		if (strcmp(entry->name, name) == 0) {
			return (1);
		}
	}

	return (0);
}

int
run_internal(int argc, char ** argv)
{

	struct pair_entry * entry;
	SLIST_FOREACH(entry, &internals_head, entries) {
		if (strcmp(entry->name, argv[0]) == 0) {
			return (*entry->function)(argc, argv);
		}
	}

	err(EX_SOFTWARE, "Invalid call to run internal. "
			"Internal does not exist.");

}

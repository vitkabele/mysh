# mysh

My private shell. [Unix/Linux Programming in C (NSWI015) assignment]

## Usage

Run as ./mysh after build and use as conventional interactive shell.

By executing as `./mysh -c "command"` only the command will be run and
by calling as `./mysh file.sh` the batch mode is executed.

## Build

Run `make` command to build binary mysh and `make clean` to cleanup the
working directory.

To run the tests, use `make test`
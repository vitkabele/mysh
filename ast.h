/*
 * This file contains definition of structs used in the abstract syntax tree
 * representing the shell line and method to proper handling those structs
 * such as creating new empty ones, adding items and freeing them.
 *
 * Created by Vít Kabele on 27/11/2018.
 */

#ifndef MYSH_AST_H
#define	MYSH_AST_H

#define	_XOPEN_SOURCE 600

#include <unistd.h>
#include <stdio.h>
#include <sys/queue.h>

#include "common.h"

/*
 * String linked list specialization
 */
STAILQ_HEAD(string_head, string_entry);


struct string_entry {
	char *	item;
	STAILQ_ENTRY(string_entry) entries;
};

/*
 * Command struct
 */
typedef struct command Command;
struct command {
	struct	string_head args;
	int		arg_count;
	int		source;
	int		destination;
	char *	srcname;
	char *	destname;
	char	append;
	char	interpretation_failure;
};

/*
 * empty_command()
 *
 * Allocate a new instance of struct command on heap
 * and sets its values to expected default values.
 * This is the only valid way how to obtain new instances
 * of command struct.
 *
 * RETURN VALUES
 * 		Returns a pointer to the allocated instance.
 *
 * ERRORS
 * 		If an error occur while allocating memory, the
 * 		program will be exited.
 *
 * WARNING
 * 		Remember to free the returned memory after use.
 */
Command *
empty_command();

/*
 * command_add_arg(command, argument)
 *
 * Add an argument to the command
 *
 * ERRORS
 * 		If an error occur while allocating memory, the
 * 		program will be exited.
 */
void
command_add_arg(Command *, const char *);

/*
 * command_add_source(command, filename)
 *
 * Add the file as an source of command.
 *
 * ERRORS
 * 		In case of not enough memory, the program will be
 * 		exited.
 */
void
command_add_source(Command *, const char *);

/*
 * command_add_destination(command, filename)
 *
 * Add the file as the destination of command.
 *
 * ERRORS
 * 		In case of not enough memory, the program will be
 * 		exited.
 */
void
command_add_destination(Command *, const char *);

/*
 * command_add_append(command, filename)
 *
 * Add the file as the destination of command.
 * The output will be appended.
 *
 * ERRORS
 * 		In case of not enough memory, the program will be
 * 		exited.
 */
void
command_add_append(Command *, const char *);

/*
 * Command linked list specialization.
 */
STAILQ_HEAD(command_head, command_entry);

struct command_entry {
	Command * command;
	STAILQ_ENTRY(command_entry) entries;
};

/*
 * Pipeline struct
 */
typedef struct pipeline Pipeline;
struct pipeline {
	struct command_head commands;
};

/*
 * empty_pipeline()
 *
 * Allocate a new instance of struct pipeline on heap
 * and sets its values to expected default values.
 * This is the only valid way how to obtain new instances
 * of pipeline struct.
 *
 * RETURN VALUES
 * 		Returns a pointer to the allocated instance.
 *
 * ERRORS
 * 		If an error occur while allocating memory, the
 * 		program will be exited.
 *
 * WARNING
 * 		Remember to free the returned memory after use.
 */
Pipeline *
empty_pipeline();

/*
 * pipeline_prepend_command(pipeline, command)
 *
 * Add a command to the pipeline
 *
 * ERRORS
 * 		If an error occur while allocating memory, the
 * 		program will be exited.
 */
void
pipeline_prepend_command(Pipeline *, Command *);

/*
 * Pipeline linked list specialization
 */
STAILQ_HEAD(pipeline_head, pipeline_entry);

struct pipeline_entry {
	Pipeline *	pipeline;
	STAILQ_ENTRY(pipeline_entry) entries;
};

/*
 * Sequence struct
 */
typedef struct sequence Sequence;
struct sequence {
	struct pipeline_head pipelines;
};

/*
 * empty_sequence()
 *
 * Allocate a new instance of struct sequence on heap
 * and sets its values to expected default values.
 * This is the only valid way how to obtain new instances
 * of sequence struct.
 *
 * RETURN VALUES
 * 		Returns a pointer to the allocated instance.
 *
 * ERRORS
 * 		If an error occur while allocating memory, the
 * 		program will be exited.
 *
 * WARNING
 * 		Remember to free the returned memory after use.
 */
Sequence *
empty_sequence();

/*
 * sequence_prepend_pipeline(sequence, pipeline)
 *
 * Add the pipeline to the sequence
 *
 * ERRORS
 * 		If an error occur while allocating memory, the
 * 		program will be exited.
 */
void
sequence_prepend_pipeline(Sequence *, Pipeline *);

/*
 * free_sequence(sequence)
 *
 * Free the whole AST.
 */
void
free_sequence(Sequence *);

#endif // MYSH_AST_H

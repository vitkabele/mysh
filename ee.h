/*
 * Execution engine of mysh.
 * Perform execution of abstract syntax tree representing command line.
 *
 * Created by Vít Kabele on 2018-12-13.
 */

#ifndef MYSH_EE_H
#define	MYSH_EE_H

#define	_XOPEN_SOURCE 600
#include <unistd.h>

#include "ast.h"

/*
 * Execute the sequence of commands represented by given AST
 */
void
execute_sequence(Sequence * sequence);

#endif // iMYSH_EE_H

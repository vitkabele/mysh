CFLAGS=-I. -Wall -pedantic
OBJ = app.o ast.o ee.o internals.o

mysh: mysh.tab.h lex.yy.c mysh.l mysh.y $(OBJ)
	$(CC) $(CFLAGS) -o $@ $(OBJ) mysh.tab.c lex.yy.c -lreadline

mysh.tab.h: mysh.y
	bison -d mysh.y

lex.yy.c: mysh.l
	flex mysh.l

clean:
	rm lex.yy.c mysh.tab.c mysh.tab.h mysh $(OBJ)

test: mysh
	./tests.sh bash
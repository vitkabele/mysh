/*
 * This file contains definition of structs used in the abstract syntax tree
 * representing the shell line and method to proper handling those structs
 * such as creating new empty ones, adding items and freeing them.
 *
 * Created by Vit Kabele on 27/11/2018.
 */

#include <stdlib.h>
#include <memory.h>
#include <assert.h>
#include <fcntl.h>

#include "ast.h"

/*
 * empty_command()
 *
 * Allocate a new instance of struct command on heap
 * and sets its values to expected default values.
 * This is the only valid way how to obtain new instances
 * of command struct.
 *
 * RETURN VALUES
 * 		Returns a pointer to the allocated instance.
 *
 * ERRORS
 * 		If an error occur while allocating memory, the
 * 		program will be exited.
 *
 * WARNING
 * 		Remember to free the returned memory after use.
 */
Command *
empty_command()
{

	CONSTRUCT(command, cmd);

	STAILQ_INIT(&cmd->args);

	cmd->destination = STDOUT_FILENO;
	cmd->source = STDIN_FILENO;
	cmd->srcname = NULL;
	cmd->destname = NULL;
	cmd->append = 0;
	cmd->interpretation_failure = 0;
	cmd->arg_count = 0;

	return (cmd);
}

/*
 * command_add_arg(command, argument)
 *
 * Add an argument to the command
 *
 * ERRORS
 * 		If an error occur while allocating memory, the
 * 		program will be exited.
 */
void
command_add_arg(Command * cmd, const char * arg)
{

	CONSTRUCT(string_entry, entry);
	entry->item = strdup(arg);

	if (STAILQ_EMPTY(&cmd->args)) {
		STAILQ_INSERT_HEAD(&cmd->args, entry, entries);
	} else {
		STAILQ_INSERT_TAIL(&cmd->args, entry, entries);
	}

	cmd->arg_count++;

}

/*
 * command_add_source(command, filename)
 *
 * Add the file as an source of command.
 *
 * ERRORS
 * 		In case of not enough memory, the program will be
 * 		exited.
 */
void
command_add_source(Command * cmd, const char * file)
{
	cmd->srcname = strdup(file);

	if (cmd->srcname == NULL) {
		err(EX_OSERR, NULL);
	}
}

/*
 * command_add_destination(command, filename)
 *
 * Add the file as the destination of command.
 *
 * ERRORS
 * 		In case of not enough memory, the program will be
 * 		exited.
 */
void
command_add_destination(Command * cmd, const char * file)
{
	cmd->destname = strdup(file);

	if (cmd->destname == NULL) {
		err(EX_OSERR, NULL);
	}

	cmd->append = 0;
}

/*
 * command_add_append(command, filename)
 *
 * Add the file as the destination of command.
 * The output will be appended.
 *
 * ERRORS
 * 		In case of not enough memory, the program will be
 * 		exited.
 */
void
command_add_append(Command * cmd, const char * file)
{
	cmd->destname = strdup(file);

	if (cmd->destname == NULL) {
		err(EX_OSERR, NULL);
	}

	cmd->append = 1;
}


/*
 * empty_pipeline()
 *
 * Allocate a new instance of struct pipeline on heap
 * and sets its values to expected default values.
 * This is the only valid way how to obtain new instances
 * of pipeline struct.
 *
 * RETURN VALUES
 * 		Returns a pointer to the allocated instance.
 *
 * ERRORS
 * 		If an error occur while allocating memory, the
 * 		program will be exited.
 *
 * WARNING
 * 		Remember to free the returned memory after use.
 */
Pipeline *
empty_pipeline()
{
	CONSTRUCT(pipeline, pipeline);
	STAILQ_INIT(&pipeline->commands);
	return (pipeline);
}

/*
 * pipeline_prepend_command(pipeline, command)
 *
 * Add a command to the pipeline
 *
 * ERRORS
 * 		If an error occur while allocating memory, the
 * 		program will be exited.
 */
void
pipeline_prepend_command(Pipeline * pipeline, Command * command)
{
	CONSTRUCT(command_entry, entry);
	entry->command = command;

	STAILQ_INSERT_HEAD(&pipeline->commands, entry, entries);
}

/*
 * empty_sequence()
 *
 * Allocate a new instance of struct sequence on heap
 * and sets its values to expected default values.
 * This is the only valid way how to obtain new instances
 * of sequence struct.
 *
 * RETURN VALUES
 * 		Returns a pointer to the allocated instance.
 *
 * ERRORS
 * 		If an error occur while allocating memory, the
 * 		program will be exited.
 *
 * WARNING
 * 		Remember to free the returned memory after use.
 */
Sequence *
empty_sequence()
{
	CONSTRUCT(sequence, sequence);
	STAILQ_INIT(&sequence->pipelines);

	return (sequence);
}

/*
 * sequence_prepend_pipeline(sequence, pipeline)
 *
 * Add the pipeline to the sequence
 *
 * ERRORS
 * 		If an error occur while allocating memory, the
 * 		program will be exited.
 */
void
sequence_prepend_pipeline(Sequence * sequence, Pipeline * pipeline)
{
	CONSTRUCT(pipeline_entry, entry);
	entry->pipeline = pipeline;

	STAILQ_INSERT_HEAD(&sequence->pipelines, entry, entries);
}

static void
free_command(Command * command)
{
	assert(command != NULL);

	struct string_entry * entry = STAILQ_FIRST(&command->args);
	struct string_entry * tmp;
	while (entry != NULL) {
		tmp = STAILQ_NEXT(entry, entries);
		free(entry->item);
		free(entry);
		entry = tmp;
	}

	free(command->destname);
	free(command->srcname);

	if (command->source != STDIN_FILENO) {
		close(command->source);
	}

	if (command->destination != STDOUT_FILENO) {
		close(command->destination);
	}

	free(command);
}

static void
free_pipeline(Pipeline * pipeline)
{
	assert(pipeline != NULL);

	struct command_entry * entry = STAILQ_FIRST(&pipeline->commands);
	struct command_entry * tmp;
	while (entry != NULL) {
		tmp = STAILQ_NEXT(entry, entries);
		free_command(entry->command);
		free(entry);
		entry = tmp;
	}

	free(pipeline);
}

/*
 * free_sequence(sequence)
 *
 * Free the whole AST.
 */
void
free_sequence(Sequence * sequence)
{
	assert(sequence != NULL);

	struct pipeline_entry * entry = STAILQ_FIRST(&sequence->pipelines);
	struct pipeline_entry * tmp;
	while (entry != NULL) {
		tmp = STAILQ_NEXT(entry, entries);
		free_pipeline(entry->pipeline);
		free(entry);
		entry = tmp;
	}

	free(sequence);
}

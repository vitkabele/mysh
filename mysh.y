%{
#include <stdio.h>
#include "app.h"
#include "ast.h"
#include "ee.h"

extern int yylineno;
extern int yylex();
extern int yyerror(const char * format);
%}

%union{
    char * str;
    Command * command;
    Pipeline * pipeline;
    Sequence * sequence;
}

%token<str> TOKEN
%token SEMICOLON EOL REDIR_OUT REDIR_IN APPEND PIPE

%type<command> action cmd
%type<pipeline> pipeline
%type<sequence> seq

%destructor { free_sequence($$); } seq

%error-verbose

%%

cmdline
    :
    | cmdline EOL
    | cmdline seq EOL { execute_sequence($2); free_sequence($2); }
    ;

seq
    : pipeline { $$ = empty_sequence(); sequence_prepend_pipeline($$, $1); }
    | pipeline SEMICOLON { $$ = empty_sequence(); sequence_prepend_pipeline($$, $1); }
    | pipeline SEMICOLON seq  { $$ = $3; sequence_prepend_pipeline($3, $1);}
    ;

pipeline
    : cmd { $$ = empty_pipeline(); pipeline_prepend_command($$, $1); }
    | cmd PIPE pipeline { $$ = $3; pipeline_prepend_command($$, $1); }
    ;

cmd
    : action
    | cmd REDIR_OUT TOKEN { command_add_destination($1,$3); }
    | cmd REDIR_IN TOKEN { command_add_source($1,$3); }
    | cmd APPEND TOKEN { command_add_append($1,$3); }
    ;

action
    : TOKEN { $$ = empty_command(); command_add_arg($$, $1);}
    | action TOKEN { command_add_arg($$, $2); }
    ;

%%

int
yyerror(const char * s){
    if (shell_state.is_batch) {
        fprintf(stderr, "error: Line %d: %s\n", yylineno, s);
    } else {
        fprintf(stderr, "error: %s\n", s);
    }
    shell_state.last_command_return_value = 2;
    return 0;
}

# Testing pipes and file redirections together

cat /etc/passwd > output | printf foobar > output2 | cat

cat output;
cat output2;

rm output;
rm output2;
